var Timeline = {

    SettingsJsonObject: {},

    progress: 0,

    /**
     * Initialise the timeline
     */
    initialise: function(progress) {
        Timeline.loadSettingsFromJson();
        Timeline.events.initialise();
        Timeline.progress = progress;
        Timeline.update();

        // Disable fullscreen on IE10 and below
        // there is separate code in VideoPlayerInterface to handle iOS
        if (Timeline.isIe10OrBelow()) {
            Timeline.disableFullScreen();
        }

        // Disable the volume slider on iOS
        if (Timeline.isiOS()) {
            Timeline.disableVolumeSlider();
        }
    },

    isIe10OrBelow: function() {
        return (navigator.userAgent.indexOf("Trident") !== -1 && navigator.userAgent.indexOf("rv:11") === -1) || navigator.appVersion.indexOf("MSIE 7.") != -1;
    },

    isiOS: function() {
        return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    },

    loadSettingsFromJson: function(){
        var settingsFileUrl = "src/settings/TimelineSettings.json";
        $.getJSON(settingsFileUrl,function (json) {
            Timeline.SettingsJsonObject = json;
            SettingsPanel.initMenu();
            // If fullscreen is disabled in TimelineSettings remove the button.
            if (!Timeline.SettingsJsonObject.FullScreenEnabled) {
                Timeline.disableFullScreen();
            }
            BufferStatus.initBuffer();
        });
    },

    disableFullScreen: function () {
        $('#jsFullScreenTimelineItem').remove();
        $('#jsSettingsTimelineItem').removeClass('col-xs-4').addClass('col-xs-6');
        $('#jsVolumeTimelineItem').removeClass('col-xs-4').addClass('col-xs-6');
    },

    disableVolumeSlider: function() {
        $("#jsVolumeTimelineItem").remove();
        $('#jsSettingsTimelineItem').removeClass('col-xs-4').addClass('col-xs-6');
        $('#jsFullScreenTimelineItem').removeClass('col-xs-4').addClass('col-xs-6');
    },

    /**
     * Set the video progress along the timeline
     */
    setProgress: function(progress) {
        Timeline.progress = progress;
        Timeline.update();
    },

    /**
     * Get the video progress along the timeline
     */
    getProgress: function() {
        return Timeline.progress;
    },

    /**
     * Update the timeline
     */
    update: function() {
        var timeline_width = $('#jsTimelineContainer').width();
        $('#jsTimelineProgress').width(timeline_width * Timeline.getProgress());
        $('#jsContrastFixedWidth').width(timeline_width);
        ContrastProgress.setContrastTimelineProgress(Timeline.getProgress());
    },

    /**
     * Update the play/pause button to have the appropriate icon
     */
    updatePlayPauseButton: function() {
        if (VideoPlayerInterface.isPlaying) {
            $('#jsPlayPauseSRText').text('Pause');
            $('#jsPlayPauseButton').removeClass('play').addClass('pause');
        } else {
            $('#jsPlayPauseSRText').text('Play');
            $('#jsPlayPauseButton').removeClass('pause').addClass('play');
        }
    },

    /**
     * Remove the timeline cover element that blocks interaction before the video is loaded
     */
    enableTimelineIfNecessary: function() {
        if ($(".timeline-cover") && VideoPlayerInterface.isSourceSet) {
            $(".timeline-cover").remove();
            $(".timeline-cover-playhead").remove();
            $(".timeline-control-button").attr('tabindex','1');
            $("#jsFullScreenButtonIcon").attr('tabindex', '3');
            $("#jsVolumeButtonIcon").attr('tabindex', '3');
            $("#jsTimelineIndicator").addClass('full-opacity');
        }
    },

    /**
     * Update the timeline state and progress
     */
    updateStateAndProgress: function(state, progress) {
        if (state == "END") {
            Timeline.setProgress(1);
            return;
        }

        if (Timeline.events.isDragging) {
            return;
        }

        var stateTimelineElem = Timeline.getStateElementByName(state);
        var totalTimelinePercent = (parseFloat(stateTimelineElem.data('percent-start')) + (parseFloat(stateTimelineElem.data('percent-width')) * progress)) / 100;
        Timeline.setProgress(totalTimelinePercent);
        Timeline.enableTimelineIfNecessary();
    },

    updateSeenChapterColors: function(){
        var currentProgress = Timeline.getProgress() * 100;
        $(".timeline-state").each(function (){
            var stateNameFriendly = $(this).data('state');
            var percentStart = parseFloat($(this).data('percent-start'));
            if(percentStart < currentProgress ){
                $('.chapter-button__' + stateNameFriendly).css('color', Timeline.SettingsJsonObject.SeenChaptersFontColor);
            }
        });
    },

    /**
     * Get the timeline state from the video progress
     */
    getStateFromProgress: function(prettyState) {
        var pc_progress = Timeline.getProgress() * 100;
        var state = 'START';
        $('.timeline-state').each(function() {
            var start = parseFloat($(this).data('percent-start'));
            var end = parseFloat($(this).data('percent-start')) + parseFloat($(this).data('percent-width'));
            if (pc_progress >= start && pc_progress < end) {
                if (typeof prettyState != 'undefined' && prettyState) {
                    state = $(this).data('state');
                } else {
                    state = VideoPlayerInterface.StateMap[$(this).data('state')];
                }
            }
        });
        return state;
    },

    /**
     * Get the progress through the current state
     */
    getProgressInState: function() {
        var stateTimelineElem = Timeline.getStateElementByName(Timeline.getStateFromProgress(true));
        return ((Timeline.getProgress() * 100) - parseFloat(stateTimelineElem.data('percent-start'))) / parseFloat(stateTimelineElem.data('percent-width'));
    },

    /**
     * Get a state HTML element by it's friendly name
     */
    getStateElementByName: function(stateName) {
        var state = null;
        $('.timeline-state').each(function(key, value) {
            if (typeof stateName != 'undefined' && stateName == $(value).data('state')) {
                state = $(this);
            }
        });
        return state;
    },

    /**
     * Update the video
     */
    updateInVideo: function() {
        var state = Timeline.getStateFromProgress();
        if (state == VideoPlayerInterface.currentState) {
            VideoPlayerInterface.actions.timelinePosition(Timeline.getProgressInState());
        } else {
            VideoPlayerInterface.actions.selectState(state);
        }
    },

    /**
     * Define the timeline events
     */
    events: {
        /**
         * Initialise events for timeline
         */
        initialise: function() {
            $('#jsTimelineContainer')
                .mousemove(Timeline.events.timelineMousemove)
                .mouseleave(Timeline.events.timelineMouseleave)
                .click(Timeline.events.timelineClick);

            $('#jsTimelineIndicator')
                .mousedown(Timeline.events.timelineIndicatorMousedown);

            $(document)
                .mouseup(Timeline.events.documentMouseup)
                .mousemove(Timeline.events.documentMousemove);

            $(window).resize(Timeline.events.windowResize);

            $('#jsPlayPauseButton').click(Timeline.events.playPauseButtonClick);

            $('#jsSkipBackButton').click(Timeline.events.skipBack);

            $('#jsSkipForwardButton').click(Timeline.events.skipForward);

            $('#jsFullScreenButton').click(Timeline.events.toggleFullscreen);
        },

        isDragging: false,

        /**
         * Show faint background when hovering over timeline.
         */
        timelineMousemove: function(e) {
            $('#jsTimelineProgressHover').width(e.pageX - $('#jsTimelineProgress').offset().left);
        },

        /**
         * Hide faint background when leaving timeline.
         */
        timelineMouseleave: function() {
            $('#jsTimelineProgressHover').width(0);
        },

        /**
         * Handle the timeline click event
         */
        timelineClick: function(e) {
            e.preventDefault();
            var container = $('#jsTimelineIndicator');
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                var timeline_width = $('#jsTimelineContainer').width();
                Timeline.setProgress((e.pageX - $('#jsTimelineProgress').offset().left) / timeline_width);
                Timeline.updateInVideo();
            }
        },

        /**
         * Handle the timeline indicator mousedown event
         */
        timelineIndicatorMousedown: function(e) {
            e.preventDefault();
            Timeline.events.isDragging = true;
        },

        /**
         * Handle the document mouseup event
         */
        documentMouseup: function(e) {
            if (Timeline.events.isDragging) {
                e.preventDefault();
                Timeline.events.isDragging = false;
                Timeline.updateInVideo();
            }
        },

        /**
         * Handle the document mousemove event
         */
        documentMousemove: function(e) {
            if (Timeline.events.isDragging) {
                var timeline_width = $('#jsTimelineContainer').width();
                var e_location = e.pageX - $('#jsTimelineProgress').offset().left;
                if (e_location >= 0 && e_location <= timeline_width) {
                    Timeline.setProgress(e_location / timeline_width);
                }
            }
        },

        /**
         * Update the timeline when the window is resized
         */
        windowResize: function() {
            Timeline.update();
        },

        /**
         * Pause/play the video when the pause/play button is clicked
         */
        playPauseButtonClick: function() {
            if (VideoPlayerInterface.isPlaying) {
                VideoPlayerInterface.actions.pause();
            } else {
                VideoPlayerInterface.actions.play();
            }
        },

        /**
         * Skip back to the last state in the video
         */
        skipBack: function() {
            VideoPlayerInterface.actions.skipBack();
        },

        /**
         * Skip forward to the next state in the video
         */
        skipForward: function() {
            VideoPlayerInterface.actions.skipForward();
        },

        toggleFullscreen: function () {
            VideoPlayerInterface.iframeWindow.rtc.player.toggleFullscreen();
        }
    }
};