var VideoPlayerInterface = {
    iframeWindow: null,

    updateInterval: null,

    RTCVisit: {},

    isPlaying: false,

    isSourceSet: false,

    StateEngine: {},

    globalVolume: 0,

    currentState: '',

    firstRun: true,

    /**
     * Define the state map that links card IDs to "friendly" names
     * This should be customised to match the interaction cards in your project
     */
    StateMap: {
        "START": "introduction",
        "c1db6805": "thanks",
        "END": "END"
    },

    pageData: {
        name: '',
        business: '',
        industryState: 'business-challenges'
    },

    /**
     * Initalise the video player interface.
     * This class is a proxy that handles all interaction with the video player itself
     */
    initialise: function() {
        VideoPlayerInterface.iframeWindow = document.getElementById("videoPlayerIframe").contentWindow;
        VideoPlayerInterface.updateFromVideo();
        VideoPlayerInterface.updateInterval = setInterval(function() {
            VideoPlayerInterface.updateFromVideo();
            BufferStatus.updateBuffer();
        }, 250);

        // Set a click handler on the resume splash screen
        $("#resumeSplash").click(function() {
            VideoPlayerInterface.actions.play();
        });

    },

    disableFullScreenOniOS: function() {
        var iosInlineFullscreenSupported = VideoPlayerInterface.iframeWindow.rtc.utils.iosInlineFullscreenSupported(document, navigator.userAgent, VideoPlayerInterface.iframeWindow.RTCConfig);
        var shouldHideFullscreenButton = VideoPlayerInterface.iframeWindow.rtc.utils.shouldHideFullscreenButton();
        // Special case to check here as iPads will always play video inline
        var isIpad = VideoPlayerInterface.iframeWindow.rtc.utils.isIpad(navigator.userAgent);
        if (shouldHideFullscreenButton && !iosInlineFullscreenSupported && !isIpad) {
            Timeline.disableFullScreen();
        }
    },

    /**
     * Get the latest video data and update all affected landing page elements.
     * This function fires at least once every second.
     */
    updateFromVideo: function() {
        try {
            if (VideoPlayerInterface.iframeWindow.rtc && VideoPlayerInterface.iframeWindow.rtc.player && VideoPlayerInterface.iframeWindow.rtc.player.playersReady()) {
                // Wait until the player is ready to initalise the quality selector
                if (!QualitySelector.loaded) {
                    QualitySelector.initialise();
                }

                if (VideoPlayerInterface.firstRun) {
                    VideoPlayerInterface.disableFullScreenOniOS();
                    VideoPlayerInterface.firstRun = false;
                }

                // Update state and timeline position
                VideoPlayerInterface.isSourceSet = VideoPlayerInterface.getSourceSet();
                VideoPlayerInterface.getStates();
                var times = VideoPlayerInterface.iframeWindow.rtc.player.getVideoTimes();
                Timeline.updateStateAndProgress(
                    VideoPlayerInterface.mapStateReverse(VideoPlayerInterface.currentState),
                    (times.play / times.status.duration) || 0
                );

                //Update Seen Chapters
                if(Timeline.SettingsJsonObject.SeenChaptersIsEnabled){
                    Timeline.updateSeenChapterColors();
                }

                //Update buffer Progress
                // BufferStatus.updateBuffer();

                // Update is playing
                VideoPlayerInterface.isPlaying = (!times.status.paused);

                // Get data from video
                VideoPlayerInterface.getVisitData();

                // Update play pause
                Timeline.updatePlayPauseButton();

                // Update resume splash screen
                VideoPlayerInterface.toggleResumeSplash();

                // Update volume slider
                var globalVolumeFromVideo = VideoPlayerInterface.iframeWindow.globalVolume;
                if (VideoPlayerInterface.globalVolume != globalVolumeFromVideo) {
                    VideoPlayerInterface.globalVolume = globalVolumeFromVideo;
                    VolumeSlider.setVolume(globalVolumeFromVideo);
                }
            }
        } catch (exception) {
            if (window.console) {
                // NOTE: This line is only for debugging.
                // TODO: Comment out or remove the line below when deploying into production
                console.error(exception);
            }
        }

        // Update play pause
        Timeline.updatePlayPauseButton();

        // Update volume slider
        var globalVolumeFromVideo = VideoPlayerInterface.iframeWindow.globalVolume;
        if (VideoPlayerInterface.globalVolume != globalVolumeFromVideo) {
            VideoPlayerInterface.globalVolume = globalVolumeFromVideo;
            VolumeSlider.setVolume(globalVolumeFromVideo);
        }

        // TODO: Need to update quality selector
    },

    /**
     * Gets the name property from data-dict-name. Updates prepared for text with name if exists
     */
    updatePreparedForName: function() {
        var nameProperty = $('#jsPerson').data('dict-name');
        var preparedName = VideoPlayerInterface.RTCVisit.videoVisitData[nameProperty];
        if(preparedName != null && preparedName.length > 0){
            if($('#jsPerson').text() !== (" " + preparedName)){
                var preparedForText = LanguageSelector.getTextByKey("PreparedForText");
                $("#jsPerson").text(preparedForText + " " + preparedName);
            }
        }
    },

    /**
     * Get the video visit data
     */
    getVisitData: function() {
        VideoPlayerInterface.RTCVisit = VideoPlayerInterface.iframeWindow.RTCVisit;
        VideoPlayerInterface.updatePreparedForName();
    },

    /**
     * Get the video states
     */
    getStates: function() {
        VideoPlayerInterface.StateEngine = VideoPlayerInterface.iframeWindow.StateEngine;
        VideoPlayerInterface.currentState = VideoPlayerInterface.iframeWindow.rtc.state.currentState();
    },

    /**
     * Map a state name onto a state object
     */
    mapState: function(state) {
        if (typeof VideoPlayerInterface.StateMap[state] == 'undefined') {
            return VideoPlayerInterface.StateMap.START;
        }
        return VideoPlayerInterface.StateMap[state];
    },

    /**
     * Map a state object onto a state name
     */
    mapStateReverse: function(state) {
        var ret = '';
        $.each(VideoPlayerInterface.StateMap, function(key, val) {
            if (val == state) {
                ret = key;
            }
        });
        return ret;
    },

    /**
     * Get the source set
     */
    getSourceSet: function() {
        try {
            if (VideoPlayerInterface.iframeWindow.rtc.player.video.status().srcSet) {
                return true;
            }
        } catch (ex) {
        }

        return false;
    },

    /**
     * Toggle the resume splash screen that appears over the video when paused
     */
    toggleResumeSplash: function() {
        var resumeVisible = $("#resumeSplash").is(":visible");
        var aboutDialogVisible = VideoPlayerInterface.iframeWindow.$("#aboutModal").is(":visible");
        var times = VideoPlayerInterface.iframeWindow.rtc.player.getVideoTimes();
        var cardsOpen = VideoPlayerInterface.iframeWindow.rtc.card.isCardVisible();

        if (!VideoPlayerInterface.isPlaying && !resumeVisible && !aboutDialogVisible && times.play > 0 && !cardsOpen) {
            VideoPlayerInterface.showResumeSplash();
        } else if((VideoPlayerInterface.isPlaying || cardsOpen) && resumeVisible) {
            VideoPlayerInterface.hideResumeSplash();
        }
    },

    /**
     * Show the resume splash screen
     */
    showResumeSplash: function() {
        $("#resumeSplash").show();
    },

    /**
     * Hide the resume splash screen
     */
    hideResumeSplash: function() {
        $("#resumeSplash").hide();
    },

    /**
     * Define the actions for the video player interface
     */
    actions: {
        skipBack: function(currentState) {
            SideButtons.closeAllSideCards();
            VideoPlayerInterface.hideResumeSplash();
            VideoPlayerInterface.iframeWindow.rtc.player.controls.rewind();
        },

        play: function() {
            SideButtons.closeAllSideCards();
            VideoPlayerInterface.hideResumeSplash();
            VideoPlayerInterface.iframeWindow.rtc.player.controls.resume();
        },

        pause: function(showResumeSplash) {
            VideoPlayerInterface.iframeWindow.rtc.player.controls.pause();

            if(showResumeSplash !== false) {
                VideoPlayerInterface.showResumeSplash();
            }
        },

        skipForward: function(currentState) {
            SideButtons.closeAllSideCards();
            VideoPlayerInterface.hideResumeSplash();
            VideoPlayerInterface.iframeWindow.rtc.player.controls.fastForward();
        },

        selectState: function(clickedState) {
            SideButtons.closeAllSideCards();
            VideoPlayerInterface.hideResumeSplash();
            VideoPlayerInterface.iframeWindow.rtc.timeline.gotoState(clickedState);
        },

        timelinePosition: function(percentage) {
            SideButtons.closeAllSideCards();
            VideoPlayerInterface.iframeWindow.$("#jquery_jplayer_videoplayer").jPlayer("playHead", percentage * 100);
        },

        volumeChange: function(vol) {
            try {
                if (isNaN(vol)) {
                    return;
                }

                if (
                    VideoPlayerInterface.iframeWindow
                    && VideoPlayerInterface.iframeWindow.rtc
                    && VideoPlayerInterface.iframeWindow.rtc.utils.storeLocal
                    && VideoPlayerInterface.iframeWindow.rtc.player.controls.changeVolume
                ) {
                    VideoPlayerInterface.iframeWindow.rtc.player.controls.changeVolume(vol);
                }
            } catch (exception) {
                if (window.console) {
                    console.error(exception); // TODO: change this line
                }
            }
        }
    }
};