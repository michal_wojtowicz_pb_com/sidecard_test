<?php
// NOTE: Include any required files here
require_once "classes/VideoGeneratorUtils.class.php";
require_once "api/SessionHelper.php";

if (isset($_GET["dev"])) {
    // Load the local App class instead of the EOV App class
    require_once "classes/App.devclass.php";
}

if (isset($_REQUEST['clear'])) {
    setSessionCookie(null);

    $location = VideoGeneratorUtils::getProtocol() . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
    $location = str_replace(array("?clear", "&clear"), "", $location);
    header("Location: " . $location);
}


// Build a query string for the embedded video
$playerQueryParams = array('_timeline' => false);

// If uid is set, create video authentication params
if (!empty($_REQUEST['uid'])) {
    require_once "classes/Authenticator.class.php";
    $authenticator = new Authenticator();

    $playerQueryParams['uid'] = $_REQUEST['uid'];
    $playerQueryParams['ts'] = time();
    $playerQueryParams['key'] = $authenticator->createAuthenticationString(
        $playerQueryParams['uid'],
        $playerQueryParams['ts']
    );
}

if (!empty($_REQUEST["c"])) {
    $playerQueryParams["c"] = $_REQUEST["c"];
}

$queryString = "?" . http_build_query($playerQueryParams);
$iframeVideoLink = "/mw_sidecard_test/videoPlayer.php{$queryString}";
//$iframeVideoLink = "https://preprod.rtcvid.net/ez_energy/videoPlayer.php{$queryString}"; // Working PURL: f19u5ap5mi132s

// If a base_url is not set in a config .ini file, we'll use this instead
$defaultBaseUrl = "http://whitelabel-landing.pb.com";
