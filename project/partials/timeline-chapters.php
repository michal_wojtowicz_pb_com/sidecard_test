<div id="jsBufferTimeline" class="buffer-container col-xs-12 nopad"></div>

<div id="jsTimelineProgressHover" class="timeline-progress-hover"></div>
<div id="jsTimelineProgress" class="timeline-progress">
    <div id="jsTimelineIndicator" class="timeline-indicator"></div>
</div>

<div id="jsTimelineContainer" class="col-xs-12 timeline-chapter-buttons nopad">

    <div id="jsNormalTimeline">
        <div class="chapter-button chapter-button__introduction">
            <!-- This chapter button only has one state associated with it -->
            <div class="timeline-state" data-percent-width="45" data-percent-start="0" data-state="introduction"></div>
            <span class="translate" data-translate="ChapterIntroduction">Clip #1</span>
        </div>

        <div class="chapter-button chapter-button__interaction">
            <div class="timeline-state" data-percent-width="10" data-percent-start="45" data-state="interaction"></div>
            <span class="translate" data-translate="Interaction">Interaction</span>
        </div>

        <div class="chapter-button chapter-button__thanks">
            <div class="timeline-state" data-percent-width="45" data-percent-start="55" data-state="thanks"></div>
            <span class="translate" data-translate="ChapterThanks">Clip #2</span>
        </div>
    </div>
</div>