<?php
require_once "partials/page-setup.php"
?><!DOCTYPE html>
<!--[if IE 7]><html class="fluidplayer ie ie7 lteie8 lteie7" lang="en"><![endif]-->
<!--[if IE 8]><html class="fluidplayer ie ie8 lteie8" lang="en"><![endif]-->
<!--[if gt IE 8]><!-->
<html class="fluidplayer" lang="en">
<!--<![endif]-->

    <head>
        <?php require_once "partials/head-meta.php" ?>
    </head>

    <body>
        <div class="full-wrapper">
            <div class="custom-container container-box">
                <header role="banner">
                    <?php require_once "partials/header.php" ?>
                </header>

                <main role="main" class="clearfix">
                    <div class="col-xs-2 side-button-container">
                        <?php require_once "partials/sidebar-left.php" ?>
                    </div>
                    <div class="col-xs-8 player-column">
                        <?php require_once "partials/player.php" ?>
                    </div>
                    <div class="col-xs-2 promos-container">
                        <?php require_once "partials/sidebar-right.php" ?>
                    </div>
                </main>

                <p class="prepared-for-text">
                    <span id="jsPerson" data-dict-name="first_name">&nbsp;</span>
                </p>

                <div class="timeline-wrapper">
                    <?php require_once "partials/timeline.php" ?>
                </div>
            </div>

            <footer role="contentinfo" class="custom-container">
                <?php require_once "partials/footer.php" ?>
            </footer>
        </div>

        <script src="dist/js/script.js"></script>

        <?php include_once "partials/google-analytics.php" ?>

    </body>
</html>
